Param(
    [Parameter(Mandatory=$true)]
    [ValidateScript({ Test-Path -Path $_ -PathType 'Container' })]
    [string]$HomeDirectory,
    [Parameter(Mandatory=$true)]
    [string]$SqlInstance
)

# Gotta do this because by default SQL Server PowerShell job steps start in SQLSERVER:
# This messes up the files moves because they're inside the wrong provider.
Set-Location C:

$VerbosePreference = 'Continue'
$ErrorActionPreference = 'Stop'

$homeDir = $HomeDirectory

$pickupDir = Join-Path -Path $homeDir -ChildPath 'Pickup'
$archiveDir = Join-Path -Path $homedir -ChildPath 'Archive'
$errorDir = Join-Path -Path $homedir -ChildPath 'ErrorArchive'

Write-Verbose "Checking pickup directory contents"
$dirContents = Get-ChildItem -Path $pickupDir

if (!$dirContents) {
    Write-Verbose "Nothing found in pickup directory. Exiting."
    exit 0
}

function Get-NextArchive ([string]$ArchivePath) {
    New-Item -ItemType Directory -Path $ArchivePath -Name ((Get-Date -Format 's') -replace ':', '')
}

# We need to find two files in the directory, both CSVs,
# one with a "GIFT No" field and one with a "PLDG No" field that does NOT have "GIFT No".
$CsvContent = @()

ForEach ($file In $dirContents) {
    if (!(Test-Path -LiteralPath $file.PSPath -PathType Leaf)) {
        throw "Pickup location contains errant folders."
    }
    $CsvContent += Import-Csv $file
}

if ($CsvContent.Count -gt 2) {
    throw "More than two files found in pickup location. Not sure what to do with this."
}

ForEach ($csv In $CsvContent) {
    if (!$csv.PSObject.Properties.Name -contains 'PLDG No') {
        throw "One of these files does not contain a PLDG No column"
    }
    if ($csv.PSObject.Properties.Name -contains 'GIFT No') {
        $GIFTS = $csv
    } else {
        $PLEDGES = $csv
    }
}

# TODO: SOMEWHERE IN HERE WE GOTTA MOVE THE FILES AROUND TO THE ARCHIVE FOLDER

# if ($dirContents.count -gt 1 -or !(Test-Path -LiteralPath $dirContents[0].PSPath -PathType Leaf)) {

#     $dirContents | Move-Item -Destination (Get-NextArchive -ArchivePath $errorDir)
#     throw "Something other than a single file was found at the Pickup location. Files have been moved to ErrorArchive."

# }

# $currentDir = Get-NextArchive -ArchivePath $archiveDir
# Write-Verbose "Moving CSV file to $($currentDir.PSPath)"
# $csvFile = $dirContents | Move-Item -Destination $currentDir -PassThru

# $outFile = Join-Path -Path $currentDir -ChildPath '_out.log'

# Write-Verbose "Loading CSV file"
# $csvData = Import-Csv -Path $csvFile

# $requiredColumns = 'BANNER_ID', 'TESSITURA_INDIVIDUAL_ID', 'TESSITURA_INDIVIDUAL_DATE'

# if (
#     ($csvData | Get-Member -Name $requiredColumns -MemberType NoteProperty
#     ).Count -ne $requiredColumns.Count
# ) {
#     throw "Import file did not contain required column headers."
# }
# if ($csvData.Count -lt 1) {
#     throw "Import file does not contain any data rows."
# }
# if (($csvData.TESSITURA_INDIVIDUAL_ID -notMatch '^[0-9]+$').count -gt 0) {
#     throw "Input file contains non-integer data in TESSITURA_INDIVIDUAL_ID"
# }

$PSDefaultParameterValues = @{
    'Invoke-SqlCmd:ServerInstance' = $SqlInstance
    'Invoke-SqlCmd:Database' = 'impresario'
    'Invoke-SqlCmd:AbortOnError' = $true
    'Invoke-SqlCmd:Verbose' = $true
    'Write-DbaDataTable:SqlInstance' = $SqlInstance
    'Write-DbaDataTable:Truncate' = $true
    'Write-DbaDataTable:Confirm' = $false
    'Write-DbaDataTable:EnableException' = $true
}

# TODO: Create new tables in bannertables database from template.
# TODO: Derive table names based on date
# TODO: Create a template table
Invoke-SqlCmd -Database 'bannertables' -Query "SELECT TOP 0 INTO #gift_data_$(Get-Date) FROM dbo.0220gift"
Invoke-SqlCmd -Database 'bannertables' -Query "SELECT TOP 0 INTO #gift_data_$(Get-Date) FROM dbo.0220pledge"

Write-Verbose "actually doing dbatools import"
# do dbatools import csv
# Import-Module -Name dbatools -Verbose:$false

Write-DbaDataTable -InputObject $GIFTS -Table = 'bannertables.dbo.gift_data_date' *>> $outFile
Write-DbaDataTable -InputObject $PLEDGES -Table = 'bannertables.dbo.pledge_data_date' *>> $outFile

Write-Verbose "the import has presumably completed without issue and we can wrap up"

# Indicate to the controller that we just loaded a new dataset.
Write-Verbose "Telling the controller proc to set the new flag and schedule an import."
Invoke-SqlCmd -Query "EXEC dbo.LP_BNGIFT_IMPORT_CONTROLLER @action = 'N';"

# Clean up our file archive
Write-Verbose "Cleaning up our Archive directory."
Get-ChildItem -Path $archiveDir |
    Where-Object -Property Name -lt ((Get-Date).AddDays(-14).ToString('s') -replace ':', '') |
    Remove-Item -Recurse -Verbose *>> $outFile
